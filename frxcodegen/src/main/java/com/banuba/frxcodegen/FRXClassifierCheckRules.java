package com.banuba.frxcodegen;

import com.banuba.frx.model.BaseStage;
import com.banuba.frx.model.Cascades;
import com.banuba.frx.model.RandomForest;
import com.banuba.frx.model.RandomTree;
import com.banuba.frx.model.StageUpdate;
import com.banuba.frx.model.UpdateValue;
import com.banuba.frx.model.Updater;

import java.util.Arrays;
import java.util.List;

import com.banuba.frxcodegen.synthesized.*;

import static com.banuba.frxcodegen.synthesized.SynthesizedUpdater.*;

public class FRXClassifierCheckRules {

    public static SynthesizedUpdater createGlobalUpdater(Cascades cascades, UpdateValue allowedUpdateValue) throws Exception {
        System.out.println("Creating Global Updater...");

        // Find first Updater and ensure all updaters in classifier are the same
        Updater firstUpdater = null;
        int firstUpdaterStageIndex = 0;

        for (int i = 0; i < cascades.stages.size(); i++) {
            BaseStage currentStage = cascades.stages.get(i);
            if (currentStage instanceof StageUpdate) {
                UpdateValue currentUpdateValue = ((StageUpdate) currentStage).update_val;
                if (currentUpdateValue != allowedUpdateValue) {
                    throw new Exception("Not allowed UpdateValue " + currentUpdateValue + " at stage " + i);
                }

                Updater currentUpdater = ((StageUpdate) currentStage).updater;
                if (firstUpdater == null) {
                    firstUpdater = currentUpdater;
                    firstUpdaterStageIndex = i;
                } else {
                    if (Arrays.equals(firstUpdater.from_offsets, currentUpdater.from_offsets) && Arrays.equals(firstUpdater.to_offsets, currentUpdater.to_offsets) && Arrays.equals(firstUpdater.types, currentUpdater.types)) {
                        // Updaters are equal
                    } else {
                        throw new Exception("Updaters are not equal for stages " + firstUpdaterStageIndex + " and " + i);
                    }
                }
            }
        }

        if (firstUpdater == null) {
            throw new Exception("No updaters found");
        }

        // Prepare separate SET and ADD lists for global updater
        SynthesizedUpdater globalUpdater = new SynthesizedUpdater();
        for (int i = 0; i < firstUpdater.types.length; i++) {
            switch (firstUpdater.types[i]) {
                case SET:
                    globalUpdater.sets.add(new UpdateOperation(firstUpdater.from_offsets[i], firstUpdater.to_offsets[i]));
                    break;
                case ADD:
                    globalUpdater.adds.add(new UpdateOperation(firstUpdater.from_offsets[i], firstUpdater.to_offsets[i]));
                    break;
                default:
                    throw new Exception("Unknown Update operation " + firstUpdater.types[i] + " at index " + i + " of stage " + firstUpdaterStageIndex);
            }
        }


        System.out.println("Global Updater created");
        return globalUpdater;
    }

    public static void fillResgressionWeights_floats(float[][] weights, float[][] weightsForFloats) throws Exception {
        int latentCount = weights.length;
        int treeLeafCount = weights[0].length;

        for (int leaf = 0; leaf < treeLeafCount; leaf++) {
            for (int latent = 0; latent < latentCount; latent++) {
                weightsForFloats[leaf][latent] = weights[latent][leaf];
            }
        }
    }

    public static void fillResgressionWeights_shorts(int[][] weights, int[][] weightsForFloats) throws Exception {
        int latentCount = weights.length;
        int treeLeafCount = weights[0].length;

        for (int leaf = 0; leaf < treeLeafCount; leaf++) {
            for (int latent = 0; latent < latentCount; latent++) {
                weightsForFloats[leaf][latent] = weights[latent][leaf];
            }
        }
    }

    public static final int TYPESIZE_FLOAT = 4;
    public static final int TYPESIZE_SHORT = 2;
    public static final int TYPESIZE_CHAR = 1;

    public static int alignSize(int typesize, int size, int alignment) {
        return ((((size*typesize) + alignment - 1) / alignment) * alignment) / typesize;
    }

    public static void convertWeightsRowToShorts(List<RandomForest> random_forests, float[] weightsRow, int[] shortWeightsRow, float[] rowScaleFactor, int maxValueLimit) throws Exception {

        // Calculate ranges for weights based on theoretically possible minimum and maximum sums.
        // Minimum case: every tree contributes its minimal weight.
        // Maximum case: every tree contributes its maximal weight.
        // As far as we perform unsaturated integer calculations on shorts, only final value range is important. Ongoing overflows and underflows are not important.
        // We also shall ensure that every value can be represented

        float minSum = 0;
        float maxSum = 0;
        float minNegativeValue = 0;
        float maxPositiveValue = 0;

        int incremental_mdl = 0;
        for (int i = 0; i < random_forests.size(); i++) {
            RandomForest forest = random_forests.get(i);

            int base = incremental_mdl;
            int forest_mdl = 0;
            for (int t = 0; t < forest.random_trees.length; t++) {
                RandomTree tree = forest.random_trees[t];

                int nonNullSplitCount = 0;
                for (int f = 0; f < tree.feats.length; f++) {
                    if (tree.feats[f] != null) {
                        nonNullSplitCount++;
                    }
                }

                int leafCount = nonNullSplitCount + 1; // Feats represent split nodes, as this is perfect tree, number of leafs is number of splits + 1 (leafCount also known aas mdl() )

                float firstVal = weightsRow[base]; // leaf 0

                float minTreeContribution = firstVal;
                float maxTreeContribution = firstVal;
                minNegativeValue = Math.min(minNegativeValue, firstVal);
                maxPositiveValue = Math.max(maxPositiveValue, firstVal);

                for (int leafIndex = 1; leafIndex < leafCount; leafIndex++) {
                    float val = weightsRow[base + leafIndex];

                    minTreeContribution = Math.min(minTreeContribution, val);
                    maxTreeContribution = Math.max(maxTreeContribution, val);
                    minNegativeValue = Math.min(minNegativeValue, val);
                    maxPositiveValue = Math.max(maxPositiveValue, val);
                }

                minSum += minTreeContribution;
                maxSum += maxTreeContribution;

                base += leafCount;
                forest_mdl += leafCount;
            }

            incremental_mdl += forest_mdl;
        }

        minSum = Math.abs(minSum);
        maxSum = Math.abs(maxSum);
        minNegativeValue = Math.abs(minNegativeValue);
        maxPositiveValue = Math.abs(maxPositiveValue);

        float range = Math.max(Math.max(minSum, maxSum), Math.max(minNegativeValue, maxPositiveValue));

        float maxValue = Math.max(minNegativeValue, maxPositiveValue);
        short preconvertedMaxValue = (short) Math.round(32767.0f * (maxValue / range));
        if (preconvertedMaxValue > maxValueLimit)
        {
            range = 32767.0f * maxValue / (float) maxValueLimit;
        }

        for (int i = 0; i < weightsRow.length; i++)
        {
            shortWeightsRow[i] = (short) Math.round(32767.0f * (weightsRow[i] / range));
        }

        rowScaleFactor[0] = range / 32767.0f;
    }

    public static void createRegressionWeightsVariants(Cascades cascades, int regressorAlignment) throws Exception {
        System.out.println("Creating Regression Weight Variants...");

        for (int i = 0; i < cascades.stages.size(); i++) {
            BaseStage currentStage = cascades.stages.get(i);
            if (currentStage instanceof StageUpdate) {
                float[][] weights = ((StageUpdate) currentStage).weights;

                int latentCount = weights.length;
                int treeLeafCount = weights[0].length;

                // Floats
                {
                    int allocatedLatentCountForFloats = alignSize(TYPESIZE_FLOAT, latentCount, regressorAlignment);
                    float[][] weightsForFloats = new float[treeLeafCount][allocatedLatentCountForFloats];
                    fillResgressionWeights_floats(weights, weightsForFloats);

                    currentStage.extent.put("latent_count", latentCount);
                    currentStage.extent.put("tree_leaf_count", treeLeafCount);
                    currentStage.extent.put("allocated_latent_count_floats", allocatedLatentCountForFloats);
                    currentStage.extent.put("weights_floats", weightsForFloats);
                }

                // Shorts
                {
                    float[] shortWeightsScaleFactors = new float[latentCount];
                    int[][] weights_short = new int[latentCount][treeLeafCount];

                    for (int l = 0; l < latentCount; l++) {
                        // Limit max value to 1023, shall not cause any value clamp loss, i.e. maximum will be purely on range basis
                        float[] latentScaleFactor = new float[1];
                        convertWeightsRowToShorts(((StageUpdate) currentStage).random_forests, weights[l], weights_short[l], latentScaleFactor, 1023);
                        shortWeightsScaleFactors[l] = latentScaleFactor[0];
                    }

                    int allocatedLatentCountForShorts = alignSize(TYPESIZE_SHORT, latentCount, regressorAlignment);
                    int[][] weightsForShorts = new int[treeLeafCount][allocatedLatentCountForShorts];
                    fillResgressionWeights_shorts(weights_short, weightsForShorts);

                    currentStage.extent.put("weights_scale_factors_shorts", shortWeightsScaleFactors);
                    currentStage.extent.put("allocated_latent_count_shorts", allocatedLatentCountForShorts);
                    currentStage.extent.put("weights_shorts", weightsForShorts);
                }

                // Chars
                {
                    float[] charWeightsScaleFactors = new float[latentCount];
                    int[][] weights_char = new int[latentCount][treeLeafCount];

                    for (int l = 0; l < latentCount; l++) {
                        // Limit max value to 127, will cause some value clamp loss, i.e. maximum will be reduced to fit maximum value into char representation
                        float[] latentScaleFactor = new float[1];
                        convertWeightsRowToShorts(((StageUpdate) currentStage).random_forests, weights[l], weights_char[l], latentScaleFactor, 127);
                        charWeightsScaleFactors[l] = latentScaleFactor[0];
                    }

                    int allocatedLatentCountForChars = alignSize(TYPESIZE_CHAR, latentCount, regressorAlignment);
                    int[][] weightsForChars = new int[treeLeafCount][allocatedLatentCountForChars];
                    fillResgressionWeights_shorts(weights_char, weightsForChars);

                    currentStage.extent.put("weights_scale_factors_chars", charWeightsScaleFactors);
                    currentStage.extent.put("allocated_latent_count_chars", allocatedLatentCountForChars);
                    currentStage.extent.put("weights_chars", weightsForChars);
                }
            }
        }

        System.out.println("Regression Weight Variants created");
    }


}
