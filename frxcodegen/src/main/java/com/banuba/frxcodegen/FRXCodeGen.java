package com.banuba.frxcodegen;

import com.banuba.frx.model.Cascades;
import com.banuba.frx.model.FaceModel;
import com.banuba.frx.model.StageUpdate;
import com.banuba.frx.model.UpdateValue;
import com.banuba.frx.reader.ClsfrReader;
import com.banuba.frxcodegen.synthesized.SynthesizedUpdater;
import com.banuba.utils.FileUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.ProviderNotFoundException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Map;

import org.stringtemplate.v4.*;

import static com.banuba.frxcodegen.FRXClassifierCheckRules.*;

public class FRXCodeGen {

    private static Path getBundleRoot(Path path) throws IOException {
        if (Files.isDirectory(path)) {
            return path;
        }

        try {
            return FileSystems.newFileSystem(path, null).getRootDirectories().iterator().next();
        } catch (ProviderNotFoundException e) {
            return null;
        }
    }

    private static Map.Entry<Path, Path> getClassifierAndAuxPaths(String path_, String auxPath_) throws IOException {
        Path path = Paths.get(path_);
        Path root = getBundleRoot(path);
        if (root == null) {
            return new AbstractMap.SimpleEntry<>(path, Paths.get(auxPath_));
        }

        return new AbstractMap.SimpleEntry<>(root.resolve(FileUtils.getFileBodyName(path) + ".model"), root.resolve("fgmodel"));
    }

    private static Map.Entry<Path, Path> getClassifierAndAuxPathsFromBundle(String path_) throws IOException {
        return getClassifierAndAuxPaths(path_, null);
    }

    public static class ClassifierContext {
        public Cascades cascades;
        public FaceModel faceModel;

        // Synthesized global objects (subject to variant-specific constrains)
        public SynthesizedUpdater globalLatentsUpdater;
    }

    public static class GeneratedFile {
        public STGroup group;
        public String templateName;
        public String fileName;
        public File fileHome;

        public GeneratedFile(STGroup group, String templateName, String fileName) {
            this.group = group;
            this.templateName = templateName;
            this.fileName = fileName;
            this.fileHome = null;
        }

        public GeneratedFile(STGroup group, String templateName, String fileName, File fileHome) {
            this.group = group;
            this.templateName = templateName;
            this.fileName = fileName;
            this.fileHome = fileHome;
        }

        public PrintWriter getPrintWriter() throws FileNotFoundException {
            if (fileHome == null)
                return new PrintWriter(fileName);
            else
                return new PrintWriter(new File(fileHome, fileName));
        }
    }

    public static String variantTemplatesFolder;

    public static STGroup createSTGroup(String fileName) {
        STGroup g = new STGroupFile(variantTemplatesFolder + "/" + fileName);
        g.registerRenderer(Number.class, new NumberRenderer());
        return g;
    }

    public static void main(String[] args) throws Exception {
        System.out.println("Hello, YVG!");

        String somePath = "train.posthub640_86_au_zero_sym_rs_03_11.extx1x16c30nl_nl_12_20h_qd_3_ltnts_symm-v40.zip";
        Map.Entry<Path, Path> _C = getClassifierAndAuxPathsFromBundle(somePath);

        Path classifierPath = _C.getKey();
        Path auxDirPath = _C.getValue();

        ClsfrReader reader = new ClsfrReader();

        Cascades cascades = reader.read(classifierPath);
        StageUpdate stage2 = (StageUpdate) cascades.stages.get(2);
        System.out.println(stage2.random_forests.size());
        System.out.println(stage2.random_forests.get(0).random_trees.length);
        //
        System.out.println(stage2.random_forests.get(0).random_trees[0].feats.length);
        System.out.println(stage2.random_forests.get(0).random_trees[0]);
        //
        // DumpUtils.dump_as_csv(new PrintWriter("dump.csv"), stage2.weights);
        // System.out.println(cascades);
        //
        FaceModel faceModel = cascades.computers.get(0).loadFaceModel(auxDirPath);
        // // System.out.println(faceModel);
        // // PrintWriter out = new PrintWriter("dump.out");
        // // out.println(faceModel);
        // // out.flush();


        ClassifierContext cc = new ClassifierContext();
        cc.cascades = cascades;
        cc.faceModel = faceModel;

        ArrayList<GeneratedFile> generatedFiles = new ArrayList<>();

//        String variant = "gen1";
        String variant = "gen2";

/*
        String regressorSubvariant = "cvmat";
        String regressorArch = "cpp";
        int regressorAlignment = 1;
*/
/*
        String regressorSubvariant = "floats";
        String regressorArch = "cpp";
        int regressorAlignment = 1;
*/
/*
        String regressorSubvariant = "shorts";
        String regressorArch = "cpp";
        int regressorAlignment = 1;
*/
/*
        String regressorSubvariant = "chars";
        String regressorArch = "cpp";
        int regressorAlignment = 1;
*/
/*
        String regressorSubvariant = "shorts";
        String regressorArch = "aarch64_neon";
        int regressorAlignment = 16;
*/

        String regressorSubvariant = "chars";
        String regressorArch = "aarch64_neon";
        int regressorAlignment = 16;

/*
        String regressorSubvariant = "shorts";
        String regressorArch = "arm_neon";
        int regressorAlignment = 16;
*/
/*
        String regressorSubvariant = "chars";
        String regressorArch = "arm_neon";
        int regressorAlignment = 16;
*/

        File generatedSrcHome = new File("/Users/user/Desktop/Projects/FRX5/facerecognitionlib/src/lbf/" + variant + "/impl");

        if ("gen1".equals(variant)) {
            variantTemplatesFolder = "frx4cppconst";

            STGroup faceModel_ConstInit_Templates = createSTGroup("faceModel_ConstInit.stg");
            STGroup regressors_ConstInit_Templates = createSTGroup("regressors_ConstInit.stg");
            STGroup trees_ConstInit_Templates = createSTGroup("trees_ConstInit.stg");
            STGroup readCascadiumTemplates = createSTGroup("constinit.stg");
            STGroup plainFileTemplates = createSTGroup("plainfiles.stg");

            generatedFiles.add(new GeneratedFile(faceModel_ConstInit_Templates, "faceModel_ConstInit_cpp_hpp", "faceModel_ConstInit.cpp.hpp", generatedSrcHome));
            generatedFiles.add(new GeneratedFile(regressors_ConstInit_Templates, "regressors_ConstInit_cpp_hpp", "regressors_ConstInit.cpp.hpp", generatedSrcHome));
            generatedFiles.add(new GeneratedFile(trees_ConstInit_Templates, "trees_ConstInit_cpp_hpp", "trees_ConstInit.cpp.hpp", generatedSrcHome));
            generatedFiles.add(new GeneratedFile(readCascadiumTemplates, "readCascadium_cpp_hpp", "readCascadium.cpp.hpp", generatedSrcHome));
            generatedFiles.add(new GeneratedFile(plainFileTemplates, "rf_cpp_hpp", "rf.cpp.hpp", generatedSrcHome));
            generatedFiles.add(new GeneratedFile(plainFileTemplates, "lbf_cpp_hpp", "lbf.cpp.hpp", generatedSrcHome));
            generatedFiles.add(new GeneratedFile(plainFileTemplates, "etter_cpp_hpp", "etter.cpp.hpp", generatedSrcHome));
        } else if ("gen2".equals(variant)) {
            variantTemplatesFolder = "frx5opt";

            // Prepare extra model info
            cc.globalLatentsUpdater = createGlobalUpdater(cc.cascades, UpdateValue.LATENTS);
            createRegressionWeightsVariants(cc.cascades, regressorAlignment);

            STGroup faceModel_ConstInit_Templates = createSTGroup("faceModel_ConstInit.stg");
            STGroup plainFileTemplates = createSTGroup("plainfiles.stg");

            generatedFiles.add(new GeneratedFile(faceModel_ConstInit_Templates, "faceModel_ConstInit_cpp_hpp", "faceModel_ConstInit.cpp.hpp", generatedSrcHome));
            generatedFiles.add(new GeneratedFile(plainFileTemplates, "rf_cpp_hpp", "rf.cpp.hpp", generatedSrcHome));
            generatedFiles.add(new GeneratedFile(plainFileTemplates, "lbf_cpp_hpp", "lbf.cpp.hpp", generatedSrcHome));
            generatedFiles.add(new GeneratedFile(plainFileTemplates, "etter_cpp_hpp", "etter.cpp.hpp", generatedSrcHome));

            STGroup LocalStageUpdate__Templates = createSTGroup("LocalStageUpdate_" + regressorSubvariant + "_" + regressorArch + ".stg");
            generatedFiles.add(new GeneratedFile(LocalStageUpdate__Templates, "readCascadium_cpp_hpp", "readCascadium.cpp.hpp", generatedSrcHome));
            generatedFiles.add(new GeneratedFile(LocalStageUpdate__Templates, "LocalStageUpdate_hpp", "LocalStageUpdate.hpp", generatedSrcHome));
            generatedFiles.add(new GeneratedFile(LocalStageUpdate__Templates, "LocalStageUpdate_impl_cpp_hpp", "LocalStageUpdate_impl.cpp.hpp", generatedSrcHome));
            generatedFiles.add(new GeneratedFile(LocalStageUpdate__Templates, "regressors_ConstInit_cpp_hpp", "regressors_ConstInit.cpp.hpp", generatedSrcHome));
            generatedFiles.add(new GeneratedFile(LocalStageUpdate__Templates, "trees_ConstInit_cpp_hpp", "trees_ConstInit.cpp.hpp", generatedSrcHome));


        } else {
            throw new Exception("Unknown CodeGen variant [" + variant + "]");
        }


        for (GeneratedFile generatedFile: generatedFiles) {
            ST st = generatedFile.group.getInstanceOf(generatedFile.templateName);
            st.add("classifier", cc);
            String result = st.render();
            PrintWriter pw = generatedFile.getPrintWriter();
            pw.println(result);
            pw.close();
        }

        System.out.println("Done");
    }

}
