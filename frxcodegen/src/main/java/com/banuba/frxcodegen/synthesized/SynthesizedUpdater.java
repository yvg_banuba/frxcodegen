package com.banuba.frxcodegen.synthesized;

import java.util.ArrayList;

public class SynthesizedUpdater {

    public static class UpdateOperation {
        public int from;
        public int to;

        public UpdateOperation(int from, int to) {
            this.from = from;
            this.to = to;
        }
    }

    public ArrayList<UpdateOperation> sets = new ArrayList<>();
    public ArrayList<UpdateOperation> adds = new ArrayList<>();

}
